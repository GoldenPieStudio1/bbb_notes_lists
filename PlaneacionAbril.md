<h1><b>Planeacion Abril</b></h1>

---

<h2><b>Index</b></h2>

- [**Arte 2D**](#arte-2d)
- [**Arte 3D**](#arte-3d)
- [**Animación**](#animaci%c3%b3n)
- [**Audio**](#audio)
- [**Programación**](#programaci%c3%b3n)
- [**Diseño**](#dise%c3%b1o)
- [**Marketing**](#marketing)
  - [**TL;DR**](#tldr)
  - [**Cosas por hacer**](#cosas-por-hacer)

---

## **Arte 2D**
  - [x] Puppet de amigo cyborg grande
  - [x] Fondo para menú modos de juego
  - [x] Botón para modos de juego en el menú principal (**reciclar a partir del de friends**), agregar chibi de amigo cyborg
  - Texturas para 5 carrocerías
    - [x] Royal / Realeza
    - [x] Paper Bus / Origami
    - [x] Crush Rig / Deshuesador
    - [x] Legendary / Legendario
    - [x] Investigator / Investigador
  - Texturas para 5 sombreros
    - [x] Crown / Corona
    - [x] Paper Hat / Marinerito
    - [x] War kid / Chico de guerra
    - [x] Pazuzu
    - [x] Intrepid / Intrépido
  - Texturas para 5 llantas
    - [x] Jewels / Joyas
    - [x] PaperBall / Bolita
    - [x] Saws / Sierras
    - [x] Pentalion
    - [x] Llantas dross (TBA)
  - Arte de gameplay para mostrar si el fin de un desafio
    - [x] Desafío cumplido
    - [x] Desafío fracasado
  - Elementos extras para HUD
    - [x] Barra modo destructor
  - Iconos para la interfaz de desafios
    - [x] Copa desafios
    - [x] Boton de Informacion
    - [x] Envolvente desafio bloqueado
    - [x] Envolvente desafio desbloqueado
    - [x] Envolvente desafio completado
  - Iconos para los desafios (Overlay)
    - [x] Reloj/Cronometro
    - [x] Los jefes (Ovni, Rexy, Motociclistas, Rival)
    - [x] Tunel
    - [x] Personita Dia de muertos
    - [x] Personita Normal
    - [x] Caja Especial (?)
    - [x] Regalos
    - [x] Props del panteon en uno solo
    - [x] 5 rexies en forma de estrellita
    - [x] Fondo del concierto de woodlove
    - [x] Icono de mach, autobus con estela de mach
    - [x] Personita Cavernicola
    - [x] 3 Personitas Juntas
    - [x] Corazon

---

## **Arte 3D**

  - Modelos para 5 sombreros (https://drive.google.com/open?id=0B1aB5KnhBJAGT1VjVUJhbzFjUjQ)
    - [x] Crown / Corona
    - [x] Paper Hat / Marinerito
    - [x] War kid / Chico de guerra (Mad Max)
    - [x] Pazuzu
    - [x] Intrepid / Intrépido
  - Modelos para 5 llantas (https://drive.google.com/open?id=0B1aB5KnhBJAGem9PdmpRSlpZSEk)
    - [x] Jewels / Joyas
    - [x] PaperBall / Bolita
    - [x] Saws / Sierras
    - [x] Pentalion
    - [x] Llantas dross (TBA)

---

## **Animación**
  - Animaciones para personaje amigo cyborg (Roby)
    - [x] Idle
    - [x] Saludo
    - [x] Hablando
    - [x] Festejando (cuando completas un desafío)
  - [ ] Pose para aparecer en banner (**FRAME**)
  - [X] Ayudar a jin para ver modelos 3D en photoshop :D

---

## **Audio**
  - 2 canciones de duración completa
    - [x] Modo Experto (Motivadora)  Deadline 20 de marzo
    - [x] Modo destructor (Heavy Metal \m/)  Deadline 31 de marzo
  - 3 canciones cortas de aprox un minuto (***DURACION POR ESPECIFICAR***)
    - [x] Carnaval/Comica   Deadline 6 de abril
    - [x] Peligro/Miedo   Deadline 13 de abril
    - [x] Reto/Enfrentamiento   Deadline 20 de abril
  - Efectos de sonido
    - [x] Para desafío completado
    - [x] Para desafío fracasado

---

## **Programación**
  - [x] Añadir modos de juego en el menú principal (**planetita**)
  - [x] Implementar el menú de modos de juego
  - [x] Implementar el submenú de desafíos
  - Crear modos de juego
    - [x] Experto
    - [x] Destructor
    - [x] Cada desafío (**Hasta ahorita 27**)
  - [ ] Implementar todos los assets gráficos y sonoros
  - [x] Implementar leaderboards

---

## **Diseño**
 - [x] Calibración de modos de juego
   - [x] Experto
   - [x] Destructor
   - [x] cada desafío (**Hasta ahorita 27**)
  - [x] Textos para menú de modos de juego y reglas de desafíos con localización (**Hasta ahorita 27**)
  - [x] Diálogos de amigo cyborg
  - [x] Definición y reglas para cada desafío (**Hasta ahorita 27**)
  - [x] Diseño de niveles para modo destructor y para cada desafío (**Hasta ahorita 27**)
  - [x] Definir desbloqueo de desafíos y modos de juego
  - [x] Definir orden en que se darán las recompensas para cada modo de juego 

---

## **Marketing**
  - Banner
    - [ ] Español
    - [ ] Ingles
  - [ ] ***Más cosas (por especificar)***
  
---

### **TL;DR**
El update va a lanzarse con:
  - 2 Nuevos modos de juego
    - Experto: Versión competitiva del modo endless.
      - No se aumenta la velocidad del camión
      - Las secciones desde un inicio son dificiles/muy dificiles
    - Destructor: Supervivencia
  - 27 nuevos desafios
  - 5 Nuevos conjuntos de camiones a desbloquear

Mas información sobre el update [**Aqui**](https://drive.google.com/open?id=1gjyXs2sO5iEZyGaTtrrypT2JVFPJJ0jx) <br>
Mas información acerca de los modos de juego [**Aqui**](https://drive.google.com/file/d/1W6_aVb8yAYF1WBpcVuu-CoC-gzMvdwJL/view?usp=sharing)  <br>
Mas información acerca de los desafios [**Aqui**](https://drive.google.com/open?id=1LAR2ow72DoBNXjmFInL9huyxCBthwu4h) <br>

### **Cosas por hacer**


