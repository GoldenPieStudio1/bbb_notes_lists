# Version 1.3.0

## Español

<es-419>

¡Feliz San Valentín!
¡Visita la tienda para descubrir todo el contenido nuevo!
   - Nueva Canción "Better days" ¡GRATIS!.
   - Autobús Hippie Wagon ¡GRATIS!
   - Nuevo Sombrero Periscopio y llantas Flower Power.
   - Muñeco Smily.
   - Ambiente WoodLove.

Enamorate con Buster Booster Bus.

</es-419>

## Ingles

<en-US>

Happy Valentine's Day! 
Check out the shop to discover all the new content!
   - New tune "Better Days" FREE!.
   - Hippie Wagon bus FREE!.
   - New periscope hat and Flower Power wheels!
   - Smiley Wobbler.
   - Woodlove stage.

Fall in love with Buster Booster Bus!

</en-US>

---------------------------------------------

# Version 1.3.1

## Español

<es-419>

Fix:
   - Arreglos visuales para WoodLove.
   - Canciones compradas en la tienda se actualizan de inmediato en playlist.

¡Feliz San Valentín!
¡Visita la tienda para descubrir todo el contenido nuevo!
   - Nueva Canción "Better days" ¡GRATIS!
   - Autobús Hippie Wagon ¡GRATIS!
   - Nuevo Sombrero Periscopio y llantas Flower Power.
   - Muñeco Smiley.
   - Ambiente WoodLove.

Enamórate con Buster Booster Bus.

</es-419>

## Ingles

<en-US>

Fix:
   - Visual fixes for WoodLove.
   - Music bought in the shop now immediately updates on the playlist.

Happy Valentine's Day! 
Check out the shop to discover all the new content!
   - New tune "Better Days" FREE!
   - Hippie Wagon bus FREE!
   - New Periscope hat and Flower Power wheels!
   - Smiley Wobbler.
   - WoodLove stage.

Fall in love with Buster Booster Bus!

</en-US>
